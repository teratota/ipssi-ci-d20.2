<?php 

namespace App\Service;

class Calculator
{
    public function add(float $a, float $b)
    {
        return $a + $b;
    }

    public function divide(float $a, float $b)
    {
        if (0 === $b) {
            throw new \DivisionByZeroError();
        }

        return $a / $b;
    }
}

